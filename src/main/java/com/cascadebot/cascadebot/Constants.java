/*
 * Copyright (c) 2019 CascadeBot. All rights reserved.
 * Licensed under the MIT license.
 */

package com.cascadebot.cascadebot;

import com.cascadebot.shared.Version;

public final class Constants {

    public static final Version CONFIG_VERSION = Version.of(1, 0, 0);

    // Changeable constants if needed
    public static final String serverInvite = "https://discord.gg/UcmXMyH";


}
