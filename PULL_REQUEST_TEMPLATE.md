# Pull request

 - [ ] I have read and agreed to the [CODE OF CONDUCT](https://github.com/CascadeBot/CascadeBot/blob/master/CODE_OF_CONDUCT.md).
 - [ ] What I'm implementing was assigned to me or was not being worked on and is a somewhat requested feature. For reference see our [trello](https://trello.com/b/gUQehy4l/bot).
 - [ ] I have tested all of my changes.
 - [ ] I have followed the style of the rest of the project.
 
## Added/Changed
What did you add and/or change for this.

## Feature
If this is a new feature, why should we add it to the bot.
